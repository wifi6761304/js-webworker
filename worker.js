console.log("Worker started");

// Das aktuelle Worker Objekt wird mit self angesprochen
// es muss in unserem Fall nicht angegeben werden.
self.onmessage = (e) => {
	console.log(e.data.message);
	const number = e.data.number;
	// Nachricht an das Browserfenster schicken
	self.postMessage(number * number);
}

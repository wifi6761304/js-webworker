self.onmessage = (e) => {
	const message = e.data.message;
	const number = e.data.number;
	console.log(message);
	const result = number * number;
	// Zurück kommunizeren über den Port, der im Event mitgegeben wurde.
	// Das versichert, dass das Event nur über das entsprechende MessageChannel Objekt
	// abegefangen werden kann. Kommunikation an alle vs. an den initialen Prozess.
	const port = e.ports[0];
	port.postMessage({message: "Worker responds", result: result});
}